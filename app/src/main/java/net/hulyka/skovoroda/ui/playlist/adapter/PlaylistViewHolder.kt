package net.hulyka.skovoroda.ui.playlist.adapter

import android.view.View
import android.widget.TextView
import kotlinx.android.synthetic.main.playlist_item.view.*

internal class PlaylistViewHolder(v: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(v) {
    val artist: TextView = v.artist
    val track: TextView = v.track
    val time: TextView = v.time
}