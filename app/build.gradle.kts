import com.android.build.gradle.api.BaseVariantOutput
import com.android.build.gradle.internal.api.BaseVariantOutputImpl
import org.gradle.internal.impldep.org.apache.ivy.util.PropertiesFile
import org.gradle.internal.impldep.org.bouncycastle.util.Properties
import org.gradle.internal.impldep.org.sonatype.maven.polyglot.groovy.builder.factory.PropertiesFactory
import org.jetbrains.kotlin.com.intellij.openapi.roots.LanguageLevelProjectExtension
import org.jetbrains.kotlin.com.intellij.pom.java.LanguageLevel
import org.jetbrains.kotlin.gradle.dsl.Coroutines.ENABLE
import org.jetbrains.kotlin.gradle.internal.AndroidExtensionsFeature
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.jetbrains.kotlin.resolve.calls.inference.newTypeInferenceOrParameterConstraintError
import org.jetbrains.kotlin.utils.newHashMapWithExpectedSize
import java.io.*
import java.util.*
import kotlin.collections.*

plugins {
    id("com.android.application")
    id("kotlin-android")
    id("kotlin-android-extensions")
    id("kotlin-kapt")
    id("io.fabric")
    id("com.google.gms.google-services")
}

apply {
    plugin("kotlin-android")
    plugin("kotlin-android-extensions")
}

androidExtensions {
    isExperimental = true
}

afterEvaluate {
    tasks.withType(KotlinCompile::class)
            .forEach {
                it.kotlinOptions { freeCompilerArgs += "-Xnew-inference" }
            }
}

kapt {
    useBuildCache = true
    mapDiagnosticLocations = true
}

android {

    compileSdkVersion(28)

    defaultConfig {
        applicationId = "net.hulyka.skovoroda"
        minSdkVersion(17)
        targetSdkVersion(28)
        versionCode = 52
        versionName = "3.5.2"
        multiDexEnabled = true
        vectorDrawables.useSupportLibrary = true
        testInstrumentationRunner = "android.support.test.runner.AndroidJUnitRunner"
        setProperty("archivesBaseName", "skovoroda-v$versionName-$versionCode")
    }

    compileOptions {
        setSourceCompatibility(JavaVersion.VERSION_1_8)
        setTargetCompatibility(JavaVersion.VERSION_1_8)
    }

    buildTypes {
        getByName("debug") {
            isMinifyEnabled = false
            isShrinkResources = false
            isZipAlignEnabled = false
            isDebuggable = true
        }
    }
}


dependencies {


    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.*"))))
    implementation(kotlin("stdlib-jdk7", "1.3.10"))
    implementation(kotlin("android-extensions-runtime", "1.3.10"))

//    implementation("androidx.appcompat:appcompat:1.0.2")
    implementation("androidx.core:core:1.0.1")
    implementation("androidx.core:core-ktx:1.0.1")
    implementation("androidx.recyclerview:recyclerview:1.0.0")
    implementation("androidx.vectordrawable:vectordrawable:1.0.1")
    implementation("androidx.constraintlayout:constraintlayout:1.1.3")
    implementation("androidx.leanback:leanback:1.0.0")
    implementation("androidx.core:core-ktx:1.0.1")
    implementation("androidx.multidex:multidex:2.0.0")
    implementation("com.google.android.material:material:1.0.0")

    // Navigation
    implementation("android.arch.navigation:navigation-fragment-ktx:1.0.0-alpha07")
    implementation("android.arch.navigation:navigation-ui-ktx:1.0.0-alpha07")

    //room
    implementation("androidx.lifecycle:lifecycle-extensions:2.0.0")
    implementation("androidx.lifecycle:lifecycle-reactivestreams:2.0.0")
    implementation("androidx.room:room-runtime:2.1.0-alpha02")
    implementation("androidx.room:room-rxjava2:2.1.0-alpha02")
    implementation("androidx.room:room-guava:2.1.0-alpha02")
    kapt("androidx.room:room-compiler:2.1.0-alpha02")

    //Rx
    implementation("io.reactivex.rxjava2:rxjava:2.2.3")
    implementation("io.reactivex.rxjava2:rxandroid:2.1.0")
    val rxbindingVersion = "3.0.0-alpha1"
    implementation("com.jakewharton.rxbinding3:rxbinding-appcompat:$rxbindingVersion")
    implementation("com.jakewharton.rxbinding3:rxbinding:$rxbindingVersion")
    val rxlifecycleVersion = "3.0.0"
    implementation("com.trello.rxlifecycle3:rxlifecycle:$rxlifecycleVersion")
    implementation("com.trello.rxlifecycle3:rxlifecycle-android-lifecycle:$rxlifecycleVersion")
    implementation("com.trello.rxlifecycle3:rxlifecycle-android-lifecycle-kotlin:$rxlifecycleVersion")

    //Retrofit
    val okhttpClientVersion = "3.12.0"
    implementation("com.squareup.okhttp3:okhttp:$okhttpClientVersion")
    implementation("com.squareup.okhttp3:logging-interceptor:$okhttpClientVersion")
    implementation("com.squareup.okio:okio:2.1.0")

    val retrofitVersion = "2.5.0"
    implementation("com.squareup.retrofit2:retrofit:$retrofitVersion")
    implementation("com.squareup.retrofit2:converter-jackson:$retrofitVersion")
    implementation("com.squareup.retrofit2:adapter-rxjava2:$retrofitVersion")

    //Jackson
    val jacksonVersion = "2.9.7"
    implementation("com.fasterxml.jackson.core:jackson-core:$jacksonVersion")
    implementation("com.fasterxml.jackson.core:jackson-databind:$jacksonVersion")
    implementation("com.fasterxml.jackson.core:jackson-annotations:$jacksonVersion")

    //Timber
    implementation("com.jakewharton.timber:timber:4.7.1")

    val daggerVersion = "2.19"
    kapt("com.google.dagger:dagger-compiler:$daggerVersion")
    kapt("com.google.dagger:dagger-android-processor:$daggerVersion")
    implementation("com.google.dagger:dagger:$daggerVersion")
    implementation("com.google.dagger:dagger-android-support:$daggerVersion")

    //Crashlytics
    implementation("com.crashlytics.sdk.android:crashlytics:2.9.6")

    //Firebase
    implementation("com.google.firebase:firebase-core:16.0.5")
    implementation("com.google.firebase:firebase-invites:16.0.5")
    implementation("com.google.firebase:firebase-perf:16.2.0")

    //Exo player
    implementation("com.google.android.exoplayer:exoplayer-core:2.9.1")
    implementation("com.google.android.exoplayer:exoplayer-ui:2.9.1")

    //billing
    implementation("com.android.billingclient:billing:1.1")

    testImplementation("junit:junit:4.12")
//    androidTestImplementation("androidx.test:runner:1.1.0-beta01")
//    androidTestImplementation("androidx.test.espresso:espresso-core:3.1.0-beta01")
//    testImplementation("org.mockito:mockito-core:2.23.0")
//    testImplementation("com.nhaarman.mockitokotlin2:mockito-kotlin:2.0.0")

}
