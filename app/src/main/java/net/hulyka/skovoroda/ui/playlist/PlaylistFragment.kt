package net.hulyka.skovoroda.ui.playlist

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_playlist.*
import net.hulyka.skovoroda.R
import net.hulyka.skovoroda.domain.base.Result
import net.hulyka.skovoroda.ui.playlist.adapter.PlaylistAdapter
import toast
import viewModelProvider
import javax.inject.Inject


class PlaylistFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: PlaylistViewModel

    private val adapter by lazy(LazyThreadSafetyMode.NONE) { PlaylistAdapter() }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_playlist, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        setupRecyclerView()
        swipeLayout.setOnRefreshListener { loadPlaylist() }

        viewModel = viewModelProvider(viewModelFactory)

        viewModel.liveData.observe(this, androidx.lifecycle.Observer {
            when (it) {
                is Result.Success -> {
                    hideProgress()
                    adapter.setData(it.data)
                }
                is Result.Loading -> {
                    showProgress()
                }
                is Result.Error -> {
                    hideProgress(); requireContext().toast("cant load feed!")
                }
            }
        })

        if (savedInstanceState == null) {
            loadPlaylist()
        }
    }

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    private fun setupRecyclerView() {
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        recyclerView.adapter = adapter
    }

    private fun showProgress() {
        swipeLayout.isRefreshing = true
    }

    private fun hideProgress() {
        swipeLayout.isRefreshing = false
    }

    private fun loadPlaylist() {
        viewModel.loadData()
    }

}
