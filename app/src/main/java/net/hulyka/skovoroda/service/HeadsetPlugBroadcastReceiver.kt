//package net.hulyka.skovoroda.service
//
//import android.content.BroadcastReceiver
//import android.content.Context
//import android.content.Intent
//import net.hulyka.skovoroda.service.PlayerService.Companion.ACTION_PAUSE
//import net.hulyka.skovoroda.service.PlayerService.Companion.ACTION_PLAY
//
///**
// * Created by mini-034 on 8/29/16.
// */
//class HeadsetPlugBroadcastReceiver : BroadcastReceiver() {
//
//    override fun onReceive(context: Context, intent: Intent) {
//        when (intent.getIntExtra(STATE, -1)) {
//            1 -> startPlayerService(context, ACTION_PLAY)
//            0 -> startPlayerService(context, ACTION_PAUSE)
//        }
//    }
//
//    private fun startPlayerService(context: Context, action: String) {
//        context.startService(PlayerService.getIntent(context, action))
//    }
//
//    companion object {
//        private const val STATE = "state"
//    }
//
//}
