package net.hulyka.skovoroda.ui.player

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import net.hulyka.skovoroda.dagger.annotation.ViewModelKey

@Module
abstract class PlayerModule {

    @Binds
    @IntoMap
    @ViewModelKey(PlayerViewModel::class)
    internal abstract fun bindPlayerViewModel(viewModel: PlayerViewModel): ViewModel
}