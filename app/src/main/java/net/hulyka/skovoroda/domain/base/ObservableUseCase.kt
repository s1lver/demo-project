package net.hulyka.skovoroda.domain.base

import io.reactivex.Observable
import net.hulyka.skovoroda.domain.base.Result.*

abstract class ObservableUseCase<in P, R> {

    protected abstract fun executeActual(parameters: P): Observable<R>

    fun executeSync(parameters: P): Observable<R> {
        return executeActual(parameters)
    }

    private fun executeAsync(parameters: P): Observable<Result<R>> {
        return executeSync(parameters)
                .map<Result<R>> {
                    Success(it)
                }
                .onErrorReturn { Error(it) }
                .startWith(Loading)
    }

    operator fun invoke(parameters: P): Observable<Result<R>> {
        return executeAsync(parameters)
    }

}

operator fun <R> ObservableUseCase<Unit, R>.invoke() = this(Unit)
operator fun <P, R> ObservableUseCase<P, R>.invoke(parameters: P) = this(parameters)