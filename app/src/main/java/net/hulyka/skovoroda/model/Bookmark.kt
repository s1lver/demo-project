package net.hulyka.skovoroda.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import java.util.*

@Entity
@TypeConverters(Converters::class)
data class Bookmark(

        @ColumnInfo(name = "artist")
        var artist: String,

        @ColumnInfo(name = "track")
        var track: String,

        @ColumnInfo(name = "created_at")
        var createdAt: Date = Date(),

        @PrimaryKey(autoGenerate = true)
        var id: Long = 0
) {
    fun getFullTitle() = "$artist $track"
}

