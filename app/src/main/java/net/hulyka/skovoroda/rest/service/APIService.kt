package net.hulyka.skovoroda.rest.service

import io.reactivex.Observable
import net.hulyka.skovoroda.rest.dto.ContentMetadataDto
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * Created by silver on 3/13/16.
 */
interface APIService {

    @GET("api/programs")
    fun getPrograms(): Call<List<ContentMetadataDto>>

    @GET("stream/current/{type}")
    fun getCurrentInfo(@Path("type") type: String): Observable<ContentMetadataDto>

    @GET("stream/list/{type}")
    fun getPlaylistInfo(@Path("type") type: String): Observable<List<ContentMetadataDto>>

}
