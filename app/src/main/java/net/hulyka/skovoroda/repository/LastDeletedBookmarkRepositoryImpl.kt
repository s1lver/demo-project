package net.hulyka.skovoroda.repository

import net.hulyka.skovoroda.model.Bookmark

class LastDeletedBookmarkRepositoryImpl : LastDeletedBookmarkRepository {

    override var lasDeletedBookmark: Bookmark? = null

}