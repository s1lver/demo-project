package net.hulyka.skovoroda.service

import EMPTY
import android.app.PendingIntent
import android.content.Context
import android.graphics.Bitmap
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.ui.PlayerNotificationManager
import net.hulyka.skovoroda.ui.player.MainActivity

class MediaDescriptionAdapterImpl(private val context: Context) :
        PlayerNotificationManager.MediaDescriptionAdapter {

    //    private val largeIcon = BitmapFactory.decodeResource(context.resources, R.drawable.ic_notification_icon)
    var contentText = EMPTY
    var contentTitle = EMPTY

    override fun createCurrentContentIntent(player: Player?): PendingIntent =
            PendingIntent.getActivity(context, 0, MainActivity.getIntent(context), 0)

    override fun getCurrentContentText(player: Player?): String? {
        return contentText
    }

    override fun getCurrentContentTitle(player: Player?): String {
        return contentTitle
    }

    override fun getCurrentLargeIcon(
            player: Player?, callback: PlayerNotificationManager.BitmapCallback?
    ): Bitmap? = null

}