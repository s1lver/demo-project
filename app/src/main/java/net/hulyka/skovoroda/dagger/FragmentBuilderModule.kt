package net.hulyka.skovoroda.dagger

import dagger.Module
import dagger.android.ContributesAndroidInjector
import net.hulyka.skovoroda.dagger.annotation.FragmentScoped
import net.hulyka.skovoroda.ui.bookmark.BookmarkFragment
import net.hulyka.skovoroda.ui.bookmark.BookmarkModule
import net.hulyka.skovoroda.ui.player.PlayerFragment
import net.hulyka.skovoroda.ui.player.PlayerModule
import net.hulyka.skovoroda.ui.playlist.PlaylistFragment
import net.hulyka.skovoroda.ui.playlist.PlaylistModule

@Module
abstract class FragmentBuilderModule {

    @FragmentScoped
    @ContributesAndroidInjector(modules = [PlayerModule::class])
    abstract fun bindPlayerActivity(): PlayerFragment

    @FragmentScoped
    @ContributesAndroidInjector(modules = [PlaylistModule::class])
    abstract fun bindPlayerFragment(): PlaylistFragment

    @FragmentScoped
    @ContributesAndroidInjector(modules = [BookmarkModule::class])
    abstract fun bindBookmarkFragment(): BookmarkFragment

}