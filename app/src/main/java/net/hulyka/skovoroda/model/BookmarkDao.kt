package net.hulyka.skovoroda.model

import androidx.room.*
import com.google.common.base.Optional
import io.reactivex.Flowable

/**
 * Created by mini-034 on 5/18/17.
 */

@Dao
interface BookmarkDao {

    @Query("SELECT * FROM bookmark WHERE artist = :artist AND track = :title")
    fun getByArtistAndTitle(artist: String, title: String): Flowable<Optional<Bookmark>>

    @Query("SELECT * FROM bookmark")
    fun getAll(): Flowable<List<Bookmark>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(bookmark: Bookmark)

    @Delete
    fun delete(bookmark: Bookmark)

}
