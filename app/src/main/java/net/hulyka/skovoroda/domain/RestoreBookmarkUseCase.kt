package net.hulyka.skovoroda.domain

import io.reactivex.Observable
import net.hulyka.skovoroda.domain.base.ObservableUseCase
import net.hulyka.skovoroda.repository.LastDeletedBookmarkRepository
import javax.inject.Inject

class RestoreBookmarkUseCase @Inject constructor(
        private val createBookmarkUseCase: CreateBookmarkUseCase,
        private val lastDeletedBookmarkRepository: LastDeletedBookmarkRepository
) : ObservableUseCase<Unit, Unit>() {

    override fun executeActual(parameters: Unit): Observable<Unit> {
        return createBookmarkUseCase.executeSync(lastDeletedBookmarkRepository.lasDeletedBookmark)
    }

}