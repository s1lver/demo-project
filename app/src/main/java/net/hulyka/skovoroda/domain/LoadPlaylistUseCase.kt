package net.hulyka.skovoroda.domain

import io.reactivex.Observable
import net.hulyka.skovoroda.domain.base.ObservableUseCase
import net.hulyka.skovoroda.preferance.PrefManager
import net.hulyka.skovoroda.rest.dto.ContentMetadataDto
import net.hulyka.skovoroda.rest.service.APIService
import javax.inject.Inject

class LoadPlaylistUseCase @Inject constructor(
        private val apiService: APIService,
        private val prefManager: PrefManager
) : ObservableUseCase<Unit, List<ContentMetadataDto>>() {

    override fun executeActual(parameters: Unit): Observable<List<ContentMetadataDto>> {
        return apiService.getPlaylistInfo(prefManager.stream.path)
    }

}