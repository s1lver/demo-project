package net.hulyka.skovoroda.ui.playlist

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import net.hulyka.skovoroda.dagger.annotation.ViewModelKey

@Module
abstract class PlaylistModule {

    @Binds
    @IntoMap
    @ViewModelKey(PlaylistViewModel::class)
    internal abstract fun bindPlaylistViewModel(viewModel: PlaylistViewModel): ViewModel

}