package net.hulyka.skovoroda.ui.bookmark.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import inflate
import net.hulyka.skovoroda.R
import net.hulyka.skovoroda.model.Bookmark

internal class BookmarkAdapter constructor(
        private val onItemClickListener: OnItemClickListener
) : ListAdapter<Bookmark, BookmarkViewHolder>(CALLBACK_DIFF) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            BookmarkViewHolder(parent.inflate(R.layout.bookmark_item))

    override fun onBindViewHolder(holder: BookmarkViewHolder, position: Int) {
        val bookmark = getItem(position)
        holder.artist.text = bookmark.artist
        holder.track.text = bookmark.track
        holder.icon.setOnClickListener { onItemClickListener.onRemoveClick(bookmark) }
        holder.root.setOnClickListener { onItemClickListener.onOpenPlayStoreClick(bookmark.getFullTitle()) }
    }

    fun setData(data: List<Bookmark>) {
        submitList(data)
    }

    interface OnItemClickListener {
        fun onRemoveClick(bookmark: Bookmark)
        fun onOpenPlayStoreClick(fullTitle: String)
    }

    companion object {

        private val CALLBACK_DIFF = object : DiffUtil.ItemCallback<Bookmark>() {
            override fun areItemsTheSame(p0: Bookmark, p1: Bookmark) = p0.id == p1.id
            override fun areContentsTheSame(p0: Bookmark, p1: Bookmark) = p0 == p1
        }

    }


}