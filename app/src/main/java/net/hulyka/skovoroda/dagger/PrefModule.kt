package net.hulyka.skovoroda.dagger

import android.content.Context
import dagger.Module
import dagger.Provides
import net.hulyka.skovoroda.preferance.PrefManager
import javax.inject.Singleton

@Module
class PrefModule {

    @Provides
    @Singleton
    fun providePreferenceManager(context: Context): PrefManager {
        return PrefManager(context.getSharedPreferences("config", Context.MODE_PRIVATE))
    }

}