package net.hulyka.skovoroda.dagger

import dagger.Module
import dagger.android.ContributesAndroidInjector
import net.hulyka.skovoroda.dagger.annotation.ActivityScoped
import net.hulyka.skovoroda.ui.player.MainActivity
import net.hulyka.skovoroda.ui.player.MainLeanbackActivity
import net.hulyka.skovoroda.ui.player.PlayerModule

@Module
abstract class ActivityBuilderModule {

    @ActivityScoped
    @ContributesAndroidInjector(modules = [PlayerModule::class])
    abstract fun bindPlayerActivity(): MainActivity

    @ActivityScoped
    @ContributesAndroidInjector(modules = [PlayerModule::class])
    abstract fun bindPlayerLeanbackActivity(): MainLeanbackActivity

}