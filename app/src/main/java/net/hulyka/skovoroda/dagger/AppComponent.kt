package net.hulyka.skovoroda.dagger

import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import net.hulyka.skovoroda.App
import javax.inject.Singleton

@Singleton
@Component(
        modules = [AppModule::class,
            NetModule::class,
            JacksonModule::class,
            RoomModule::class,
            PrefModule::class,
            ViewModelModule::class,
            RepositoryModule::class,
            ActivityBuilderModule::class,
            FragmentBuilderModule::class,
            ServiceBuilderModule::class,
            AndroidSupportInjectionModule::class]
)

interface AppComponent : AndroidInjector<App> {
    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<App>()
}