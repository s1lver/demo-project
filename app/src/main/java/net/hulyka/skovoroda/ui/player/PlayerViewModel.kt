package net.hulyka.skovoroda.ui.player

import addTo
import androidx.lifecycle.LiveData
import androidx.lifecycle.LiveDataReactiveStreams
import androidx.lifecycle.MutableLiveData
import com.google.common.base.Optional
import net.hulyka.skovoroda.dagger.rx.SchedulersFacade
import net.hulyka.skovoroda.domain.CreateBookmarkUseCase
import net.hulyka.skovoroda.domain.LoadBookmarkByArtistAndTitleUseCase
import net.hulyka.skovoroda.domain.RemoveBookmarkUseCase
import net.hulyka.skovoroda.domain.base.Result
import net.hulyka.skovoroda.model.Bookmark
import net.hulyka.skovoroda.player.State
import net.hulyka.skovoroda.player.StreamType
import net.hulyka.skovoroda.repository.NowPlayingMetaRepository
import net.hulyka.skovoroda.rest.dto.ContentMetadataDto
import net.hulyka.skovoroda.viewmodel.BaseViewModel
import javax.inject.Inject

class PlayerViewModel @Inject constructor(
        private val schedulersFacade: SchedulersFacade,
        private val loadBookmarkUseCase: LoadBookmarkByArtistAndTitleUseCase,
        private val removeBookmarkUseCase: RemoveBookmarkUseCase,
        private val createBookmarkUseCase: CreateBookmarkUseCase,
        nowPlayingMetaRepository: NowPlayingMetaRepository
) : BaseViewModel() {

    val bookmarkExistLiveData = MutableLiveData<Result<Optional<Bookmark>>>()

    val nowPlayingMeta: LiveData<Pair<State, Map<StreamType, ContentMetadataDto>>> =
            LiveDataReactiveStreams.fromPublisher(nowPlayingMetaRepository.playerState)

    private var currentBookmark: Bookmark? = null

    fun checkIfBookmarkExist(bookmark: Bookmark) {
        currentBookmark = bookmark
        loadBookmarkUseCase(bookmark)
                .subscribeOn(schedulersFacade.io())
                .observeOn(schedulersFacade.ui())
                .subscribe { bookmarkExistLiveData.value = it }
                .addTo(disposable)
    }

    fun removeBookmark(bookmark: Bookmark) {
        removeBookmarkUseCase(bookmark)
                .subscribeOn(schedulersFacade.io())
                .observeOn(schedulersFacade.ui())
                .subscribe { }
                .addTo(disposable)
    }

    fun createBookmark() {
        createBookmarkUseCase(currentBookmark)
                .subscribeOn(schedulersFacade.io())
                .observeOn(schedulersFacade.ui())
                .subscribe { }
                .addTo(disposable)
    }


}