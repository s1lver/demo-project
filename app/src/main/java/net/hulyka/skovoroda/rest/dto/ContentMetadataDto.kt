package net.hulyka.skovoroda.rest.dto

import EMPTY
import android.os.Parcelable
import androidx.annotation.Keep
import kotlinx.android.parcel.Parcelize
import java.text.SimpleDateFormat
import java.util.*

@Keep
@Parcelize
data class ContentMetadataDto(
        var id: Int = 0,
        var artist: String = EMPTY,
        var track: String = EMPTY,
        var createdAt: Date = Date()
) : Parcelable {

    val title: String
        get() = "$artist ${if (artist.isEmpty() || track.isEmpty()) "" else " - "} $track"

    val formattedDate: String
        get() = TIME_SDF.format(createdAt)

    companion object {
        val NO_DATA = ContentMetadataDto()
        private val TIME_SDF = SimpleDateFormat("HH:mm", Locale.ROOT)
    }

}
