package net.hulyka.skovoroda.ui.bookmark.adapter

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import kotlinx.android.synthetic.main.bookmark_item.view.*

internal class BookmarkViewHolder(v: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(v) {
    val root: View = v.root
    val artist: TextView = v.artist
    val track: TextView = v.track
    val icon: ImageView = v.icon
}