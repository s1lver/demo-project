package net.hulyka.skovoroda.repository

import io.reactivex.Flowable
import io.reactivex.subjects.Subject
import net.hulyka.skovoroda.model.Bookmark
import net.hulyka.skovoroda.player.State
import net.hulyka.skovoroda.player.StreamType
import net.hulyka.skovoroda.rest.dto.ContentMetadataDto

interface LastDeletedBookmarkRepository {

    var lasDeletedBookmark: Bookmark?

}