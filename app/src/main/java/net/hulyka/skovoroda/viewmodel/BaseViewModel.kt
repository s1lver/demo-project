package net.hulyka.skovoroda.viewmodel

import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable

open class BaseViewModel : ViewModel() {

    internal val disposable = CompositeDisposable()

    override fun onCleared() {
        disposable.dispose()
    }

}