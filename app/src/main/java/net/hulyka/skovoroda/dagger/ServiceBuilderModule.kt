package net.hulyka.skovoroda.dagger

import dagger.Module
import dagger.android.ContributesAndroidInjector
import net.hulyka.skovoroda.service.PlayerService
import net.hulyka.skovoroda.service.PlayerServiceModule

@Module
abstract class ServiceBuilderModule {

    @ContributesAndroidInjector(modules = [PlayerServiceModule::class])
    abstract fun bindPlayerService(): PlayerService

}