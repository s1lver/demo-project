package net.hulyka.skovoroda.preferance

import android.content.SharedPreferences
import net.hulyka.skovoroda.player.StreamType

class PrefManager(private val pref: SharedPreferences) {

    var stream: StreamType
        set(value) = pref.edit().putInt(PREF_STREAM_TYPE, value.ordinal).apply()
        get() = StreamType.values()[pref.getInt(PREF_STREAM_TYPE, 0)]

    companion object {
        private const val PREF_STREAM_TYPE = "stream_type_id"
    }

}


