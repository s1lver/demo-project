package net.hulyka.skovoroda.domain

import com.google.common.base.Optional
import io.reactivex.Observable
import net.hulyka.skovoroda.domain.base.ObservableUseCase
import net.hulyka.skovoroda.model.Bookmark
import net.hulyka.skovoroda.model.RoomDb
import javax.inject.Inject

class LoadBookmarkByArtistAndTitleUseCase @Inject constructor(
        private val room: RoomDb
) : ObservableUseCase<Bookmark, Optional<Bookmark>>() {

    override fun executeActual(parameters: Bookmark): Observable<Optional<Bookmark>> {
        return room
                .bookmarkDao()
                .getByArtistAndTitle(parameters.artist, parameters.track)
                .toObservable()
    }

}