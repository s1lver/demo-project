package net.hulyka.skovoroda.ui.playlist

import addTo
import androidx.lifecycle.MutableLiveData
import net.hulyka.skovoroda.dagger.rx.SchedulersFacade
import net.hulyka.skovoroda.domain.LoadPlaylistUseCase
import net.hulyka.skovoroda.domain.base.Result
import net.hulyka.skovoroda.domain.base.invoke
import net.hulyka.skovoroda.rest.dto.ContentMetadataDto
import net.hulyka.skovoroda.viewmodel.BaseViewModel
import javax.inject.Inject

class PlaylistViewModel @Inject constructor(
        private val loadPlaylistUseCase: LoadPlaylistUseCase,
        private val schedulersFacade: SchedulersFacade
) : BaseViewModel() {

    val liveData = MutableLiveData<Result<List<ContentMetadataDto>>>()

    internal fun loadData() {
        loadPlaylistUseCase()
                .subscribeOn(schedulersFacade.io())
                .observeOn(schedulersFacade.ui())
                .subscribe {
                    liveData.setValue(it)
                }
                .addTo(disposable)
    }

}