package net.hulyka.skovoroda.model

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [Bookmark::class], version = 1)
abstract class RoomDb : RoomDatabase() {

    abstract fun bookmarkDao(): BookmarkDao

}