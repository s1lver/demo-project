package net.hulyka.skovoroda.dagger

import dagger.Module
import dagger.Provides
import net.hulyka.skovoroda.repository.LastDeletedBookmarkRepository
import net.hulyka.skovoroda.repository.LastDeletedBookmarkRepositoryImpl
import net.hulyka.skovoroda.repository.NowPlayingMetaRepository
import net.hulyka.skovoroda.repository.NowPlayingMetaRepositoryImpl
import net.hulyka.skovoroda.rest.service.APIService
import javax.inject.Singleton

@Module
class RepositoryModule {

    @Provides
    @Singleton
    fun provideNowPlayingMetaRepository(apiService: APIService): NowPlayingMetaRepository {
        return NowPlayingMetaRepositoryImpl(apiService)
    }

    @Provides
    @Singleton
    fun provideLastDeletedBookmarkRepository(): LastDeletedBookmarkRepository {
        return LastDeletedBookmarkRepositoryImpl()
    }

}