package net.hulyka.skovoroda.repository

import io.reactivex.Flowable
import io.reactivex.subjects.Subject
import net.hulyka.skovoroda.player.State
import net.hulyka.skovoroda.player.StreamType
import net.hulyka.skovoroda.rest.dto.ContentMetadataDto

interface NowPlayingMetaRepository {

    val contentMetadata: Subject<Map<StreamType, ContentMetadataDto>>
    val state: Subject<State>
    val playerState: Flowable<Pair<State, Map<StreamType, ContentMetadataDto>>>

    fun startUpdateContentTask()

    fun stopUpdateContentTask()
}