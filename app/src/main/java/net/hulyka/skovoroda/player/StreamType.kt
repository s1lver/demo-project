package net.hulyka.skovoroda.player

import androidx.annotation.StyleRes

import net.hulyka.skovoroda.R

/**
 * Created by mini-034 on 9/6/16.
 */
enum class StreamType(
        val url: String,
        val path: String,
        @field:StyleRes val themeResId: Int
) {

    RADIO(
            "http://stream.radioskovoroda.com:8000/radioskovoroda",
            "radio",
            R.style.AppThemeGreen
    ),
    MUSIC(
            "http://stream.radioskovoroda.com:8060/skovoroda_music",
            "music",
            R.style.AppThemeYellow
    );

}
