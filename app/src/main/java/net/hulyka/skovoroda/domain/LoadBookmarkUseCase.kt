package net.hulyka.skovoroda.domain

import io.reactivex.Observable
import net.hulyka.skovoroda.domain.base.ObservableUseCase
import net.hulyka.skovoroda.model.Bookmark
import net.hulyka.skovoroda.model.RoomDb
import javax.inject.Inject

class LoadBookmarkUseCase @Inject constructor(
        private val room: RoomDb
) : ObservableUseCase<Unit, List<Bookmark>>() {

    override fun executeActual(parameters: Unit): Observable<List<Bookmark>> {
        return room
                .bookmarkDao()
                .getAll()
                .toObservable()
    }

}