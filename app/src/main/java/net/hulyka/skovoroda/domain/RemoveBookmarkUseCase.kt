package net.hulyka.skovoroda.domain

import io.reactivex.Observable
import net.hulyka.skovoroda.domain.base.ObservableUseCase
import net.hulyka.skovoroda.model.Bookmark
import net.hulyka.skovoroda.model.RoomDb
import net.hulyka.skovoroda.repository.LastDeletedBookmarkRepository
import javax.inject.Inject

class RemoveBookmarkUseCase @Inject constructor(
        private val room: RoomDb,
        private val lastDeletedBookmarkRepository: LastDeletedBookmarkRepository
) : ObservableUseCase<Bookmark, Unit>() {

    override fun executeActual(parameters: Bookmark): Observable<Unit> {
        return Observable.fromCallable {
            room.bookmarkDao().delete(parameters)
            lastDeletedBookmarkRepository.lasDeletedBookmark = parameters
        }
    }

}