package net.hulyka.skovoroda.ui.bookmark

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import dagger.android.support.AndroidSupportInjection
import getColorPrimary
import kotlinx.android.synthetic.main.fragment_bookmarks.*
import net.hulyka.skovoroda.R
import net.hulyka.skovoroda.domain.base.Result
import net.hulyka.skovoroda.model.Bookmark
import net.hulyka.skovoroda.ui.bookmark.adapter.BookmarkAdapter
import toast
import viewModelProvider
import javax.inject.Inject


class BookmarkFragment : Fragment(), BookmarkAdapter.OnItemClickListener {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: BookmarkViewModel

    private val adapter: BookmarkAdapter by lazy(LazyThreadSafetyMode.NONE) {
        BookmarkAdapter(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_bookmarks, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        setupRecyclerView()

        viewModel = viewModelProvider(viewModelFactory)


        viewModel.bookmarksLiveData.observe(this, Observer {
            when (it) {
                is Result.Success -> adapter.setData(it.data)
                is Result.Loading -> requireContext().toast("Loading...")
                is Result.Error -> requireContext().toast("Can not load bookmarks!")
            }
        })

        viewModel.bookmarkDeleteLiveData.observe(this, Observer {
            when (it) {
                is Result.Success -> showUndoSnack()
                is Result.Loading -> requireContext().toast("Deleting...")
                is Result.Error -> requireContext().toast("Can not delete bookmark!")
            }
        })

        if (savedInstanceState == null) {
            viewModel.loadBookmarks()
        }

    }

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }


    private fun setupRecyclerView() {
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        recyclerView.adapter = adapter
    }

    override fun onRemoveClick(bookmark: Bookmark) {
        viewModel.removeBookmark(bookmark)
    }

    override fun onOpenPlayStoreClick(fullTitle: String) {
        startActivity(createPlayMarketIntent(fullTitle))
    }

    private fun createPlayMarketIntent(content: String): Intent {
        val query = "search?q=$content&c=music"
        val i = Intent(Intent.ACTION_VIEW, Uri.parse("market://$query"))
        if (i.resolveActivity(requireContext().packageManager) == null) {
            i.data = Uri.parse("https://play.google.com/store/$query")
        }
        return i
    }

    private fun showUndoSnack() {
        Snackbar.make(recyclerView, "Bookmark removed", Snackbar.LENGTH_LONG)
                .setActionTextColor(requireContext().getColorPrimary())
                .setAction("UNDO") { viewModel.restoreBookmark() }
                .show()
    }

}
