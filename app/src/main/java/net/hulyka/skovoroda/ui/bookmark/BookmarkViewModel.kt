package net.hulyka.skovoroda.ui.bookmark

import addTo
import androidx.lifecycle.MutableLiveData
import net.hulyka.skovoroda.dagger.rx.SchedulersFacade
import net.hulyka.skovoroda.domain.LoadBookmarkUseCase
import net.hulyka.skovoroda.domain.RemoveBookmarkUseCase
import net.hulyka.skovoroda.domain.RestoreBookmarkUseCase
import net.hulyka.skovoroda.domain.base.Result
import net.hulyka.skovoroda.domain.base.invoke
import net.hulyka.skovoroda.model.Bookmark
import net.hulyka.skovoroda.viewmodel.BaseViewModel
import javax.inject.Inject

class BookmarkViewModel @Inject constructor(
        private val loadBookmarkUseCase: LoadBookmarkUseCase,
        private val removeBookmarkUseCase: RemoveBookmarkUseCase,
        private val restoreBookmarkUseCase: RestoreBookmarkUseCase,
        private val schedulersFacade: SchedulersFacade
) : BaseViewModel() {

    val bookmarksLiveData = MutableLiveData<Result<List<Bookmark>>>()
    val bookmarkDeleteLiveData = MutableLiveData<Result<Unit>>()

    fun loadBookmarks() {
        loadBookmarkUseCase()
                .subscribeOn(schedulersFacade.io())
                .observeOn(schedulersFacade.ui())
                .subscribe { bookmarksLiveData.setValue(it) }
                .addTo(disposable)
    }

    fun removeBookmark(bookmark: Bookmark) {
        removeBookmarkUseCase(bookmark)
                .subscribeOn(schedulersFacade.io())
                .observeOn(schedulersFacade.ui())
                .subscribe {
                    bookmarkDeleteLiveData.value = it
                }
                .addTo(disposable)
    }

    fun restoreBookmark() {
        restoreBookmarkUseCase()
                .subscribeOn(schedulersFacade.io())
                .observeOn(schedulersFacade.ui())
                .subscribe({}, {})
                .addTo(disposable)
    }

}