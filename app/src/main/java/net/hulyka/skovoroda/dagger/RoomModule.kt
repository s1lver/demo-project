package net.hulyka.skovoroda.dagger

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import net.hulyka.skovoroda.model.RoomDb
import javax.inject.Singleton

@Module
class RoomModule {

    @Provides
    @Singleton
    fun provideAppDatabase(context: Context): net.hulyka.skovoroda.model.RoomDb {
        return Room
                .databaseBuilder(context, RoomDb::class.java, "skovoroda-app-db")
                .build()
    }

}