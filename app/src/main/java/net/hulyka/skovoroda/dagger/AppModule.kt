package net.hulyka.skovoroda.dagger

import android.content.Context
import dagger.Binds
import dagger.Module
import net.hulyka.skovoroda.App
import javax.inject.Singleton

@Module
abstract class AppModule {

    @Binds
    @Singleton
    abstract fun bindContext(context: App): Context

}