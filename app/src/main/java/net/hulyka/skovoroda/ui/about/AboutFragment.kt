package net.hulyka.skovoroda.ui.about

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager.HORIZONTAL
import androidx.recyclerview.widget.PagerSnapHelper
import kotlinx.android.synthetic.main.fragment_about.*
import kotlinx.android.synthetic.main.simple_grid_item.view.*
import net.hulyka.skovoroda.R


class AboutFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_about, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setupRecyclerView()
    }

    private fun setupRecyclerView() {
        recyclerView.layoutManager =
                LinearLayoutManager(requireContext(), HORIZONTAL, false)
        recyclerView.adapter = ScheduleAdapter()
        PagerSnapHelper().attachToRecyclerView(recyclerView)
    }

    private class ScheduleAdapter : androidx.recyclerview.widget.RecyclerView.Adapter<ScheduleAdapter.ScheduleViewHolder>() {

        private val imageRes = intArrayOf(
                R.drawable.about_01,
                R.drawable.about_02,
                R.drawable.about_03,
                R.drawable.about_04,
                R.drawable.about_05,
                R.drawable.about_06
        )

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ScheduleViewHolder {
            return ScheduleViewHolder(
                    LayoutInflater.from(parent.context).inflate(
                            R.layout.simple_grid_item,
                            parent,
                            false
                    )
            )
        }

        override fun onBindViewHolder(holder: ScheduleViewHolder, position: Int) {
            holder.image.setImageResource(imageRes[position])
        }

        override fun getItemCount(): Int {
            return imageRes.size
        }

        internal inner class ScheduleViewHolder(v: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(v) {
            val image: ImageView = v.image
        }

    }

}
