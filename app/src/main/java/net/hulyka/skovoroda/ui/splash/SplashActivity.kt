package net.hulyka.skovoroda.ui.splash

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import net.hulyka.skovoroda.ui.player.MainActivity

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        startActivity(MainActivity.getIntent(this))
        finish()
    }

}
