package net.hulyka.skovoroda.model

import androidx.room.TypeConverter
import java.util.*

/**
 * Created by mini-034 on 6/26/17.
 */

class Converters {

    @TypeConverter
    fun fromTimestamp(value: Long?): Date? {
        return if (value == null) null else Date(value)
    }

    @TypeConverter
    fun dateToTimestamp(date: Date?): Long? {
        return date?.time
    }

}
