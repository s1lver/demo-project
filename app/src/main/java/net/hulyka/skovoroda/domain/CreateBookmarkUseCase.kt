package net.hulyka.skovoroda.domain

import io.reactivex.Observable
import net.hulyka.skovoroda.domain.base.ObservableUseCase
import net.hulyka.skovoroda.model.Bookmark
import net.hulyka.skovoroda.model.RoomDb
import javax.inject.Inject

class CreateBookmarkUseCase @Inject constructor(
        private val room: RoomDb
) : ObservableUseCase<Bookmark?, Unit>() {

    override fun executeActual(parameters: Bookmark?): Observable<Unit> {
        if (parameters == null) return Observable.just(Unit)
        return Observable.fromCallable {
            room.bookmarkDao().insert(parameters)
        }
    }

}