package net.hulyka.skovoroda.repository

import addTo
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import net.hulyka.skovoroda.player.State
import net.hulyka.skovoroda.player.StreamType
import net.hulyka.skovoroda.rest.dto.ContentMetadataDto
import net.hulyka.skovoroda.rest.service.APIService
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class NowPlayingMetaRepositoryImpl @Inject constructor(
        private val apiService: APIService
) : NowPlayingMetaRepository {

    override val contentMetadata = BehaviorSubject.create<Map<StreamType, ContentMetadataDto>>()

    override val state = BehaviorSubject.createDefault(State.IDLE)

    override val playerState: Flowable<Pair<State, Map<StreamType, ContentMetadataDto>>> =
            Observable.combineLatest(state, contentMetadata,
                    BiFunction { t1, t2 -> Pair(t1, t2) })
                    .toFlowable(BackpressureStrategy.LATEST)

    private var updateContentTaskDisposable = CompositeDisposable()

    override fun startUpdateContentTask() {
        Observable
                .interval(15, TimeUnit.SECONDS, Schedulers.io())
                .startWith(0)
                .flatMap { _ ->
                    Observable.range(0, StreamType.values().size)
                            .map { StreamType.values()[it] }
                            .flatMap { streamType ->
                                apiService.getCurrentInfo(streamType.path)
                                        .flatMap { Observable.just(mapOf(streamType to it)) }
                                        .startWith(mapOf(streamType to ContentMetadataDto.NO_DATA))
                                        .subscribeOn(Schedulers.io())
                            }
                            .reduce { t1: Map<StreamType, ContentMetadataDto>, t2: Map<StreamType, ContentMetadataDto> ->
                                mutableMapOf<StreamType, ContentMetadataDto>().apply {
                                    putAll(t1)
                                    putAll(t2)
                                }
                            }.toObservable()
                }.subscribe({ contentMetadata.onNext(it) }, Timber::e)
                .addTo(updateContentTaskDisposable)
    }

    override fun stopUpdateContentTask() {
        updateContentTaskDisposable.dispose()
        updateContentTaskDisposable = CompositeDisposable()
    }

}