package net.hulyka.skovoroda.ui.player

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.ui.setupActionBarWithNavController
import com.android.billingclient.api.*
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.activity_main.*
import net.hulyka.skovoroda.BuildConfig
import net.hulyka.skovoroda.R
import net.hulyka.skovoroda.preferance.PrefManager
import toPx
import javax.inject.Inject


open class MainActivity : AppCompatActivity(), HasSupportFragmentInjector,
        PurchasesUpdatedListener, BillingClientStateListener {

    @Inject
    lateinit var fragmentInjector: DispatchingAndroidInjector<Fragment>

    @Inject
    lateinit var prefManager: PrefManager

    private lateinit var mBillingClient: BillingClient

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        setTheme(prefManager.stream.themeResId)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupActionBar()
        setupNavView()
        setupActionBarWithNavController(findNavController(R.id.nav_host_fragment), drawerLayout)
        findNavController(R.id.nav_host_fragment).addOnNavigatedListener { _, d ->
            if (d.id == R.id.playerFragment) {
                ViewCompat.setElevation(appBarLayout, 0f)
            } else {
                ViewCompat.setElevation(appBarLayout, resources.getDimension(R.dimen.padding_16dp).toPx)
            }
        }

        openFacebookButton.setOnClickListener { openUri(Uri.parse(FACEBOOK_URL)) }
        openTwitterButton.setOnClickListener { openUri(Uri.parse(TWITTER_URL)) }
        openInstagramButton.setOnClickListener { openUri(Uri.parse(INSTAGRAM_URL)) }

        donateRadio.setOnClickListener { openUri(Uri.parse(DONATE_URL)) }
        donateApp.setOnClickListener { openDonateApp() }
        footerItem2.setOnClickListener { openUri(Uri.parse(RELEVANT_URL)) }

        mBillingClient = BillingClient
                .newBuilder(this)
                .setListener(this)
                .build()
                .apply { startConnection(this@MainActivity) }

    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> = fragmentInjector


    private fun setupActionBar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(true)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (mBillingClient.isReady) {
            mBillingClient.endConnection()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        return if (findNavController(R.id.nav_host_fragment).currentDestination?.id == findNavController(R.id.nav_host_fragment).graph.startDestination) {
            drawerLayout.openDrawer(GravityCompat.START); true
        } else {
            findNavController(R.id.nav_host_fragment).navigateUp()
        }
    }

    private fun setupNavView() {
        navView.setNavigationItemSelectedListener { item ->
            drawerLayout.closeDrawer(GravityCompat.START)
            when (item.itemId) {
                R.id.nav_playlist ->
                    findNavController(R.id.nav_host_fragment).navigate(R.id.action_playerFragment_to_playlistFragment)
                R.id.nav_about ->
                    findNavController(R.id.nav_host_fragment).navigate(R.id.action_playerFragment_to_aboutFragment)
                R.id.nav_bookmarks ->
                    findNavController(R.id.nav_host_fragment).navigate(R.id.action_playerFragment_to_bookmarkFragment)
            }
            true
        }
    }

    override fun onBillingSetupFinished(responseCode: Int) {
        if (responseCode == BillingClient.BillingResponse.OK) {
            mBillingClient.queryPurchases(BillingClient.SkuType.INAPP)
                    .purchasesList
                    .orEmpty()
                    .forEach { consumePayment(it.purchaseToken) }
        }
    }

    override fun onBillingServiceDisconnected() {
        mBillingClient.startConnection(this)
    }

    override fun onPurchasesUpdated(responseCode: Int, purchases: MutableList<Purchase>?) {
        if (responseCode == BillingClient.BillingResponse.OK) {
            purchases
                    .orEmpty()
                    .forEach { consumePayment(it.purchaseToken) }
        }
    }

    private fun consumePayment(token: String) {
        mBillingClient.consumeAsync(token) { _, _ -> }
    }

    private fun openDonateApp() {
        val flowParams = BillingFlowParams.newBuilder()
                .setSku(SKU_DONATION)
                .setType(BillingClient.SkuType.INAPP)
                .build()
        mBillingClient.launchBillingFlow(this, flowParams)
    }

    private fun openUri(uri: Uri) {
        startActivity(Intent(Intent.ACTION_VIEW, uri))
    }

    companion object {
        private val SKU_DONATION = if (BuildConfig.DEBUG) "android.test.purchased" else "app.donation"

        private const val FACEBOOK_URL = "https://www.facebook.com/radioskovoroda"
        private const val TWITTER_URL = "https://twitter.com/RadioSkovoroda"
        private const val INSTAGRAM_URL = "https://instagram.com/radioskovoroda"
        private const val LIQPAY_DATA =
                "eyJ2ZXJzaW9uIjozLCJhY3Rpb24iOiJwYXlkb25hdGUiLCJwdWJsaWNfa2V5IjoiaTg2MTQwMzY4ODAiLCJhbW91bnQiOiIxMDAiLCJjdXJyZW5jeSI6IlVBSCIsImRlc2NyaXB0aW9uIjoi0JHQu9Cw0LPQvtC00ZbQudC90LjQuSDQstC90LXRgdC+0LouINCU0Y/QutGD0ZTQvNC+ISIsInR5cGUiOiJkb25hdGUiLCJsYW5ndWFnZSI6ImVuIn0="
        private const val LIQPAY_SIGNATURE = "ZatkrM8ceItVoEza7J/W6Dkr1dE="
        private const val DONATE_URL =
                "https://www.liqpay.com/api/3/checkout?data=$LIQPAY_DATA&signature=$LIQPAY_SIGNATURE"
        private const val RELEVANT_URL = "https://relevant.software"

        fun getIntent(context: Context): Intent = Intent(context, MainActivity::class.java)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
    }

}
