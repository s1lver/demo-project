package net.hulyka.skovoroda.ui.bookmark

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import net.hulyka.skovoroda.dagger.annotation.ViewModelKey

@Module
abstract class BookmarkModule {

    @Binds
    @IntoMap
    @ViewModelKey(BookmarkViewModel::class)
    internal abstract fun bindBookmarkViewModel(viewModel: BookmarkViewModel): ViewModel

}