package net.hulyka.skovoroda.player

/**
 * Created by silver on 4/3/16.
 */
enum class State {

    IDLE,
    PREPARING,
    PLAYING

}
