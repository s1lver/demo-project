package net.hulyka.skovoroda

import android.content.Context
import android.os.Looper
import android.os.StrictMode
import androidx.multidex.MultiDex
import com.crashlytics.android.Crashlytics
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.exceptions.UndeliverableException
import io.reactivex.plugins.RxJavaPlugins
import net.hulyka.skovoroda.BuildConfig.DEBUG
import net.hulyka.skovoroda.dagger.DaggerAppComponent
import net.hulyka.skovoroda.timber.CrashlyticsTree
import timber.log.Timber
import java.io.IOException
import java.net.SocketException

class App : DaggerApplication() {


    override fun onCreate() {
        setupStrictMode()
        super.onCreate()
        setupRxErrorHandler()
        setupRxMainThreadScheduler()
        setupTimber()
        Crashlytics.getInstance().core.setBool("debug", BuildConfig.DEBUG)
    }

    private fun setupStrictMode() {
        if (DEBUG) {
            StrictMode.setThreadPolicy(
                    StrictMode.ThreadPolicy.Builder()
                            .detectDiskReads()
                            .detectDiskWrites()
                            .detectNetwork()
                            .penaltyLog()
                            .build()
            )
            StrictMode.setVmPolicy(
                    StrictMode.VmPolicy.Builder()
                            .detectLeakedSqlLiteObjects()
                            .detectLeakedClosableObjects()
                            .penaltyLog()
                            .penaltyDeath()
                            .build()
            )
        }
    }


    private fun setupRxErrorHandler() {
        RxJavaPlugins.setErrorHandler { e ->
            when (e) {
                is UndeliverableException ->
                    Timber.w(e, "Undeliverable exception received. Need investigation!")
                is IOException, is SocketException -> return@setErrorHandler
                is InterruptedException -> return@setErrorHandler
                else -> {
                    Thread.currentThread()
                            .uncaughtExceptionHandler
                            .uncaughtException(Thread.currentThread(), e)
                }
            }
        }
    }

    private fun setupRxMainThreadScheduler() {
        RxAndroidPlugins.setInitMainThreadSchedulerHandler {
            AndroidSchedulers.from(Looper.getMainLooper(), true)
        }
    }

    private fun setupTimber() {
        if (DEBUG) {
            Timber.plant(Timber.DebugTree())
        } else {
            Timber.plant(CrashlyticsTree())
        }
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent.builder().create(this)
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

}
