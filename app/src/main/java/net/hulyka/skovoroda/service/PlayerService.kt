package net.hulyka.skovoroda.service

import addTo
import android.app.Notification
import android.app.Service
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Binder
import androidx.core.app.NotificationCompat
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.Player.*
import com.google.android.exoplayer2.audio.AudioAttributes
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.source.TrackGroupArray
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.trackselection.TrackSelectionArray
import com.google.android.exoplayer2.ui.PlayerNotificationManager
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory
import dagger.android.AndroidInjection
import io.reactivex.disposables.CompositeDisposable
import net.hulyka.skovoroda.R
import net.hulyka.skovoroda.player.State
import net.hulyka.skovoroda.player.StreamType
import net.hulyka.skovoroda.preferance.PrefManager
import net.hulyka.skovoroda.repository.NowPlayingMetaRepository
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by silver on 3/11/16.
 */
class PlayerService : Service() {

    @Inject
    lateinit var prefManager: PrefManager

    @Inject
    lateinit var nowPlayingMetaRepository: NowPlayingMetaRepository

    private lateinit var player: SimpleExoPlayer
    private lateinit var playerNotificationManager: PlayerNotificationManager
    private lateinit var mediaDescriptionAdapter: MediaDescriptionAdapterImpl

    private val disposable = CompositeDisposable()

    override fun onCreate() {
        AndroidInjection.inject(this)
        super.onCreate()
        player = ExoPlayerFactory.newSimpleInstance(this, DefaultTrackSelector())
                .apply {
                    setAudioAttributes(AudioAttributes.DEFAULT, true)
                    addListener(object : Player.EventListener {
                        override fun onPlaybackParametersChanged(playbackParameters: PlaybackParameters) {}

                        override fun onSeekProcessed() {}

                        override fun onTracksChanged(
                                trackGroups: TrackGroupArray,
                                trackSelections: TrackSelectionArray
                        ) {
                        }

                        override fun onPlayerError(error: ExoPlaybackException) {
                            Timber.e(error)
                        }

                        override fun onLoadingChanged(isLoading: Boolean) {}

                        override fun onPositionDiscontinuity(reason: Int) {}

                        override fun onRepeatModeChanged(repeatMode: Int) {}

                        override fun onShuffleModeEnabledChanged(shuffleModeEnabled: Boolean) {}

                        override fun onTimelineChanged(
                                t: Timeline?,
                                manifest: Any?,
                                reason: Int
                        ) {
                        }

                        override fun onPlayerStateChanged(
                                playWhenReady: Boolean,
                                playbackState: Int
                        ) {
                            when (playbackState) {
                                STATE_IDLE -> {
                                    player.playWhenReady = false
                                    nowPlayingMetaRepository.state.onNext(State.IDLE)
                                }
                                STATE_ENDED -> nowPlayingMetaRepository.state.onNext(State.IDLE)
                                STATE_BUFFERING -> nowPlayingMetaRepository.state.onNext(State.PREPARING)
                                STATE_READY -> {
                                    if (playWhenReady) {
                                        nowPlayingMetaRepository.state.onNext(State.PLAYING)
                                    } else {
                                        nowPlayingMetaRepository.state.onNext(State.IDLE)
                                    }
                                }
                            }
                        }
                    })
                }

        mediaDescriptionAdapter = MediaDescriptionAdapterImpl(this)

        playerNotificationManager = PlayerNotificationManager.createWithNotificationChannel(
                this,
                CHANNEL_ID, R.string.notification_channel, NOTIFICATION_ID, mediaDescriptionAdapter
        )
                .apply {
                    setSmallIcon(R.drawable.ic_notification_icon)
                    setBadgeIconType(NotificationCompat.BADGE_ICON_NONE)
                    setUseNavigationActions(false)
                    setFastForwardIncrementMs(0)
                    setRewindIncrementMs(0)
                    setNotificationListener(object : PlayerNotificationManager.NotificationListener {
                        override fun onNotificationCancelled(notificationId: Int) {
                            stopForeground(true)
                            nowPlayingMetaRepository.stopUpdateContentTask()
                        }

                        override fun onNotificationStarted(
                                notificationId: Int,
                                notification: Notification
                        ) {
                            startForeground(NOTIFICATION_ID, notification)
                            nowPlayingMetaRepository.startUpdateContentTask()

                        }
                    })
                    setPlayer(player)

                }

        nowPlayingMetaRepository.playerState.subscribe({
            val metadata = it.second[prefManager.stream]
            val newContentText = metadata?.track.orEmpty()
            val newContentTitle = metadata?.artist.orEmpty()
            if (mediaDescriptionAdapter.contentText != newContentText || mediaDescriptionAdapter.contentTitle != newContentTitle) {
                mediaDescriptionAdapter.contentText = newContentText
                mediaDescriptionAdapter.contentTitle = newContentTitle
                playerNotificationManager.invalidate()
            }
        }, Timber::w)
                .addTo(disposable)
    }

    override fun onStartCommand(
            i: Intent?,
            flags: Int,
            startId: Int
    ): Int {
        when (i?.action) {
            ACTION_TOGGLE -> toggle()
            ACTION_UPDATE_STREAM -> setStreamType(prefManager.stream)
        }
        return START_STICKY
    }

    private fun toggle() {
        if (player.playbackState == Player.STATE_IDLE) {
            setStreamType(prefManager.stream)
        }
        player.playWhenReady = !player.playWhenReady
    }

    private fun setStreamType(streamType: StreamType) {
        val mediaSource =
                ExtractorMediaSource.Factory(DefaultHttpDataSourceFactory("skovoroda"))
                        .createMediaSource(Uri.parse(streamType.url))
        player.prepare(mediaSource)
    }

    override fun onDestroy() {
        disposable.dispose()
        playerNotificationManager.setPlayer(null)
        player.release()
        super.onDestroy()
    }

    override fun onBind(intent: Intent?): Binder? = null

    companion object {

        const val ACTION_TOGGLE = "net.hulyka.skovoroda.ACTION_TOGGLE"
        const val ACTION_UPDATE_STREAM = "net.hulyka.skovoroda.ACTION_UPDATE_STREAM"

        private const val NOTIFICATION_ID = 1233
        private const val CHANNEL_ID = "media_playback_channel"

        fun getIntent(
                context: Context,
                action: String? = null
        ): Intent {
            return Intent(context, PlayerService::class.java).also {
                it.action = action
            }
        }

    }

}
