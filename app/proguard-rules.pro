# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /home/silver/opt/android-sdk-linux/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.bak.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
# This is a configuration file for ProGuard.
# http://proguard.sourceforge.net/index.html#manual/usage.html

# Kotlin
-dontwarn kotlin.**
-keep class kotlin.Metadata { *; }
-dontwarn kotlin.reflect.jvm.internal.**
-keep class kotlin.reflect.jvm.internal.** { *; }
-keepclassmembers class **$WhenMappings {
    <fields>;
}
-keepattributes EnclosingMethod
-keepattributes InnerClasses


# Remove Android logging calls
-assumenosideeffects class android.util.Log {
    public static boolean isLoggable(java.lang.String, int);
    public static int v(...);
    public static int intent(...);
    public static int w(...);
    public static int d(...);
    public static int e(...);
}

# RemoteViews might need annotations.
-keepattributes *Annotation*

# Preserve all fundamental application classes.
-keep public class * extends android.app.Activity
-keep public class * extends android.app.Application
-keep public class * extends android.app.Service
-keep public class * extends android.content.BroadcastReceiver
-keep public class * extends android.content.ContentProvider
-keep public class * extends android.app.backup.BackupAgentHelper
-keep public class * extends android.preference.Preference

# Preserve the required interface from the License Verification Library
# (but don't nag the developer if the library is not used at all).
-keep public interface com.android.vending.licensing.ILicensingService
-dontnote com.android.vending.licensing.ILicensingService

# Keep all models
-keepclassmembers class net.hulyka.skovoroda.rest.dto.** { *; }

#Sliding Panel
-keep class com.sothree.slidinguppanel.** { *; }
-dontwarn com.sothree.slidinguppanel.**

# OkHttp 3
-dontwarn okhttp3.**
-dontwarn okio.**
-dontwarn javax.annotation.**
-keepnames class okhttp3.internal.publicsuffix.PublicSuffixDatabase

#Retrofit
-dontwarn retrofit2.Platform$Java8
-dontnote retrofit2.Platform
-keepattributes Signature
-keepattributes Exceptions

# Produces useful obfuscated stack traces
-renamesourcefileattribute SourceFile
-keepattributes SourceFile, LineNumberTable

# Crashlitics
-keep class com.crashlytics.** { *; }
-dontwarn com.crashlytics.**
-keep public class * extends java.lang.Exception

#-keep public class com.google.android.gms.* { public *; }
#-dontwarn com.google.android.gms.**

#Guava
# Guava (official)
## Not yet defined: follow https://github.com/google/guava/issues/2117
# Guava (unofficial)
## https://github.com/google/guava/issues/2926#issuecomment-325455128
## https://stackoverflow.com/questions/9120338/proguard-configuration-for-guava-with-obfuscation-and-optimization
-dontwarn com.google.common.base.**
-dontwarn com.google.errorprone.annotations.**
-dontwarn com.google.j2objc.annotations.**
-dontwarn java.lang.ClassValue
-dontwarn org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement
# Added for guava 23.5-android
-dontwarn afu.org.checkerframework.**
-dontwarn org.checkerframework.**

# Keep Jackson stuff
-keepnames class com.fasterxml.jackson.** { *; }
-dontwarn com.fasterxml.jackson.databind.**
-keep class org.codehaus.** { *; }
-dontwarn org.w3c.dom.bootstrap.DOMImplementationRegistry
-keepattributes Annotation,EnclosingMethod,Signature
-keepclassmembers public final enum org.codehaus.jackson.annotate.JsonAutoDetect$Visibility {
    public static final org.codehaus.jackson.annotate.JsonAutoDetect$Visibility *;
 }
