package net.hulyka.skovoroda.ui.playlist.adapter

import android.view.ViewGroup
import inflate
import net.hulyka.skovoroda.R
import net.hulyka.skovoroda.rest.dto.ContentMetadataDto
import java.util.*

internal class PlaylistAdapter : androidx.recyclerview.widget.RecyclerView.Adapter<PlaylistViewHolder>() {

    private val data = ArrayList<ContentMetadataDto>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            PlaylistViewHolder(parent.inflate(R.layout.playlist_item))

    override fun onBindViewHolder(holder: PlaylistViewHolder, position: Int) {
        val metadata = data[position]
        holder.time.text = metadata.formattedDate
        holder.artist.text = metadata.artist
        holder.track.text = metadata.track
    }

    override fun getItemCount() = data.size

    fun setData(data: List<ContentMetadataDto>) {
        this.data.clear()
        this.data.addAll(data)
        notifyDataSetChanged()
    }
}