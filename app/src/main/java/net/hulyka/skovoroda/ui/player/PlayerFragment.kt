package net.hulyka.skovoroda.ui.player

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.exoplayer2.util.Util
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.activity_player.*
import net.hulyka.skovoroda.R
import net.hulyka.skovoroda.domain.base.Result
import net.hulyka.skovoroda.model.Bookmark
import net.hulyka.skovoroda.player.State
import net.hulyka.skovoroda.player.StreamType
import net.hulyka.skovoroda.preferance.PrefManager
import net.hulyka.skovoroda.service.PlayerService
import viewModelProvider
import javax.inject.Inject


class PlayerFragment : Fragment() {

    @Inject
    lateinit var prefManager: PrefManager

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: PlayerViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_player, container, false)
    }

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setupStreamSwitcher()
        playerActivityPlayPause.setOnClickListener { startPlayerService(PlayerService.ACTION_TOGGLE) }

        viewModel = viewModelProvider(viewModelFactory)

        viewModel.bookmarkExistLiveData.observe(this, Observer { result ->
            if (result is Result.Success && result.data.isPresent) {
                favorite.setImageResource(R.drawable.ic_favorite_accent_36dp)
                favorite.setOnClickListener { viewModel.removeBookmark(result.data.get()) }
            } else {
                favorite.setImageResource(R.drawable.ic_favorite_border_accent_36dp)
                favorite.setOnClickListener { viewModel.createBookmark() }
            }
        })

        viewModel.nowPlayingMeta.observe(this, Observer { result ->
            when (result.first) {
                State.PLAYING -> {
                    playerActivityProgress.visibility = View.INVISIBLE
                    playerActivityPlayPause.setImageResource(R.drawable.pause)
                }
                State.IDLE -> {
                    playerActivityProgress.visibility = View.INVISIBLE
                    playerActivityPlayPause.setImageResource(R.drawable.play)
                }
                State.PREPARING -> {
                    playerActivityProgress.visibility = View.VISIBLE
                }
            }
            result.second[prefManager.stream]?.let {
                artist.text = it.artist
                track.text = it.track
                favorite.visibility = View.VISIBLE
                viewModel.checkIfBookmarkExist(Bookmark(it.artist, it.track))
            }
        })
    }

    private fun setupStreamSwitcher() {
        groupBtn.position = prefManager.stream.ordinal
        groupBtn.setOnPositionChangedListener { _, currentPosition, _ ->
            if (prefManager.stream.ordinal == currentPosition) return@setOnPositionChangedListener
            prefManager.stream = StreamType.values()[currentPosition]
            startPlayerService(PlayerService.ACTION_UPDATE_STREAM)
            requireActivity().recreate()
        }
    }

    private fun startPlayerService(action: String) {
        Util.startForegroundService(requireActivity(), PlayerService.getIntent(requireActivity(), action))
    }


}
